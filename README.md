# realSH

A code generator for fast spherical harmonics evaluation with explicit AVX2, AVX512 ...

```
usage: SHEval_code_generator.py [-h] [--language_type LANGUAGE_TYPE]
                                [--do_deriv DO_DERIV] [--phase PHASE]
                                [--total_lmax TOTAL_LMAX]

efficient real spherical harmonics (Ylm) code generator

optional arguments:
  -h, --help            show this help message and exit
  --language_type LANGUAGE_TYPE
                        language and type of the code:(Fortran_single,Fortran_
                        double,Fortran_quadruple,c++,c++_avx2,c++_avx512)
  --do_deriv DO_DERIV   do also derivatives of Ylm:(True/False)
  --phase PHASE         type of phase for Ylm:(aims,Condon-Shortley,None)
  --total_lmax TOTAL_LMAX
                        largest lmax to be generated
```
